package test01_lambda;

import test01_lambda.framework.Func01;

public class Main {

	public static void main(String[] args) {
		Func01 func01 = () -> System.out.println("hoge");
		func01.doSomethind();
	}

}
