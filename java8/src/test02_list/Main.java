package test02_list;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<String> strList = Arrays.asList("hoge", "piyo", "fuga");
		strList.forEach(s -> {
			System.out.println(s);
		});
	}

}
